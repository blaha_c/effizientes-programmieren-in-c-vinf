//
// Created by ArbeitsPC on 02.07.2021.
//
/*
 * A binary tree can represented with an 2D-array aArray[level][order]
 * Level represents the height of the binary tree, order the position to the parent.
 * With a special value like 0 we can mark position and leaf of the nodes.
 * For marking the leafs we need one level more than the nodes in fact need.
 * Alternative we can use a 2D-array of a struct containing node value and leaf information, too.
 */
#include <stdio.h>
#include <stdlib.h>

void buildLevels(int **binaryTree, int height){
    if (height > 0) {
        int a = 0;
        binaryTree[a++] = (int *)calloc(1, sizeof(int));
        for (int i = a; i < height + 1; i++) {
            binaryTree[i] = (int *)calloc(i * 2, sizeof(int));
        }
    }
}

void fillLevelOrder(int **binaryTree, int *values, int cntValues){
    if (cntValues > 0) {
        int height = 1, temp = cntValues - 1;
        while (temp > 0){
            temp /= 2;
            ++height;
        }
        int a = 0, b = 0, k = 0, levelAmount = 1;
        binaryTree[a++][b] = values[k++];
        for (int i = a; i < height - 1; i++) {
            levelAmount *= 2;
            for (int j = b; j < levelAmount; j++) {
                binaryTree[i][j] = values[k++];
            }
        }
    }
}

int main(){
    int *binaryTree1[5];
    int *binaryTree2[5];
    buildLevels(binaryTree1, 4);
    buildLevels(binaryTree2, 4);

    int valuesLevelOrder1[] = {42, 17, 2, 12, 21, 15, 10, 0, 0, 0, 0, 0, 0, 0, 14};
    int valuesLevelOrder2[] = {42, 0, 2, 0, 0, 15, 10, 0, 0, 0, 0, 0, 0, 0, 14};
    fillLevelOrder(binaryTree1, valuesLevelOrder1, sizeof(valuesLevelOrder1)/sizeof(int));
    fillLevelOrder(binaryTree2, valuesLevelOrder1, sizeof(valuesLevelOrder2)/sizeof(int));

    printf("%d\n", binaryTree1[0][0]);
    printf("%d\n", binaryTree1[1][0]);
    printf("%d\n", binaryTree1[3][7]);

    return 0;
}