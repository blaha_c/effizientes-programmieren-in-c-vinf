//
// Created by ArbeitsPC on 23.06.2021.
//
#include "Uebung07A2.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

char *encodeDoublets(char *str) {
    //Own tolower() implementation:
    //char *lowerStr = toLower(str);

    //Inspired by: https://www.tutorialspoint.com/c_standard_library/c_function_toupper.htm
    int i = 0;
    char *lowerStr;
    strcpy (lowerStr, str);
    //toupper()/tolower() Works only with ASCII character - no ÄäÖö and so forth
    while(lowerStr[i]) {
        lowerStr[i] = tolower(lowerStr[i]);
        i++;
    }

    int a[256];
    //Fill all memory spaces of array a[] with 0.
    for (int i = 0; i<256; i++){
        a[i] = 0;
    }
    //Count equal characters
    for(int i = 0; i < strlen(lowerStr); i++){
        a[lowerStr[i]] += 1;
    }

    char *returnStr = malloc(strlen(lowerStr)+1);
    for(int i = 0; i < strlen(lowerStr); i++){
        if(a[lowerStr[i]] >= 2){
            returnStr[i] = ')';
        }else{
            returnStr[i] = '(';
        }
    }
    returnStr[strlen(lowerStr)] = '\0';

    return returnStr;
}

char *toLower(char *str) {
    char *rStr = malloc((strlen(str) + 1) * sizeof(char));
    strcpy (rStr, str);
    //Resource: https://www.tutorialspoint.com/c-program-for-lowercase-to-uppercase-and-vice-versa
    for (int i = 0; rStr[i]!='\0'; i++) {
        if(rStr[i] >= 'A' && rStr[i] <= 'Z') {  //For toUpper() only replace 'A','Z' with 'a','z'
            rStr[i] = rStr[i] + 32;             //and '+ 32' with '-32'.
        }
    }
    return rStr;
}