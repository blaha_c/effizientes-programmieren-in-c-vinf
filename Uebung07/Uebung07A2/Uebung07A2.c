//
// Created by ArbeitsPC on 23.06.2021.
//
#include "Uebung07A2.h"
#include <stdio.h>
int main() {
    char str1[] = "din";
    char str2[] = "recede";
    char str3[] = "Success";
    char str4[] = "(( @";
    char str5[] = "aAzZäÄß";;

    char *str6 = encodeDoublets(str1);
    char *str7 = encodeDoublets(str2);
    char *str8 = encodeDoublets(str3);
    char *str9 = encodeDoublets(str4);
    char *str10 = encodeDoublets(str5);

    printf("%s - %s\n", str1, str6);
    printf("%s - %s\n", str2, str7);
    printf("%s - %s\n", str3, str8);
    printf("%s - %s\n", str4, str9);
    printf("%s - %s\n", str5, str10);

    return 0;
}