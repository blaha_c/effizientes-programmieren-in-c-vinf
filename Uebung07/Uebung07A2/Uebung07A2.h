//
// Created by ArbeitsPC on 23.06.2021.
//

#ifndef EFFIZIENTES_PROGRAMMIEREN_IN_C_VINF_UEBUNG07A2_H
#define EFFIZIENTES_PROGRAMMIEREN_IN_C_VINF_UEBUNG07A2_H

extern char *encodeDoublets(char *str);
extern char *toLower(char *str);

#endif //EFFIZIENTES_PROGRAMMIEREN_IN_C_VINF_UEBUNG07A2_H
