//
// Created by ArbeitsPC on 23.06.2021.
//
#include "Uebung07A1.h"
#include <stdio.h>

int main() {
    char name[] = " Mein Name ist Blaha Christian! ";
    char *withoutSpace = removeSpaces(name);
    printf("-%s-\n", withoutSpace);

    return 0;
}