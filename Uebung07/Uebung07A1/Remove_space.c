//
// Created by ArbeitsPC on 23.06.2021.
//
#include "Uebung07A1.h"

char *removeSpaces(char *inStr) {
    //Inpired by: https://stackoverflow.com/questions/1726302/removing-spaces-from-a-string-in-c
    char *returnStr = inStr;
    char *itStr = inStr;
    do {
        while (*itStr == ' ') {
            ++itStr;
        }
    } while (*inStr++ = *itStr++);
    return returnStr;
}