//
// Created by ArbeitsPC on 23.06.2021.
//
#include "Uebung07A3.h"
#include <stdio.h>
//Question 3:   Think about suitable test runs and use them to test the software.
//Answer 3:     str[0 - 20]
/*
 * Disclaimer: sometimes (aperiodic) ocours the error: "Process finished with exit code -1073741819 (0xC0000005)"
 */
int main() {
    char *strK[22];
    char *str[22];
    str[0] = "abcd~efd";
    str[1] = "abcd~~efd";
    str[2] = "abcd~~~efd";
    str[3] = "abcd~~~~efd";
    str[4] = "abcd~~~~~efd";
    str[5] = "abcdaefd";
    str[6] = "abcdaaefd";
    str[7] = "abcdaaaefd";
    str[8] = "abcdaaaaefd";
    str[9] = "abcdaaaaaefd";
    str[10] = "a~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~a";     //95x'~'
    str[11] = "a~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~a";    //96x'~'
    str[12] = "a~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~a";   //97x'~'
    str[13] = "a~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~a";  //98x'~'
    str[14] = "a~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~a"; //99x'~'
    str[15] = "abbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbba";     //95x'b'
    str[16] = "abbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbba";    //96x'b'
    str[17] = "abbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbba";   //97x'b'
    str[18] = "abbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbba";  //98x'b'
    str[19] = "abbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbba"; //99x'b'
    str[20] = "abbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbba";//100x'b'
    str[21] = "~ao~~aao~~~aaao~~~~aaaao~~~~~aaaaao";

    for(int i = 0; i < 22; i++) {
        strK[i] = doRLC(str[i]);
        puts(strK[i]);
    }

    return 0;
}