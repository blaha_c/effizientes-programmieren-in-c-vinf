//
// Created by ArbeitsPC on 23.06.2021.
//
#include "Uebung07A3.h"
#include <stdlib.h>
//Question 1:   What is the maximum length of the result string?
//Answer 1:     int n = strlen(str); strlen(rStr) == n/2 + (n/2 + n%2) * 2
//Example 1:    ~a~a~a~ -> ~~a~~a~~a~~; 7 -> 11
//Question 2:   Think about which subtasks you can divide the doRLC()-function, program the helper methods first and test them.
//Answer 2:     ???
char *doRLC(char *str){
    char *compressedReturnStr = (char *)malloc(sizeof(char));
    int j = 0, cnt = 1;

    for (int i = 0; str[i]!='\0'; i++) {
        if(str[i] == str[i+1]){
            ++cnt;
        } else{
            while(cnt > 95) {
                compressedReturnStr = realloc(compressedReturnStr, (j + 4) * sizeof(char));
                compressedReturnStr[j++] = '~';
                compressedReturnStr[j++] = (char) (95 + ' ' - 2);
                compressedReturnStr[j++] = str[i];
                cnt -= 95;
            }
            if(cnt == 1 && str[i] == '~'){
                compressedReturnStr = realloc(compressedReturnStr, (j + 3) * sizeof(char));
                compressedReturnStr[j++] = str[i];
                compressedReturnStr[j++] = str[i];
            } else if(cnt < 4 && str[i] != '~'){
                compressedReturnStr = realloc(compressedReturnStr, (j + cnt) * sizeof(char));
                for(int k = 0; k < cnt; k++){
                    compressedReturnStr[j++] = str[i];
                }
                cnt = 1;
            } else {
                compressedReturnStr = realloc(compressedReturnStr, (j + 4) * sizeof(char));
                compressedReturnStr[j++] = '~';
                compressedReturnStr[j++] = (char) (cnt + ' ' - 2);
                compressedReturnStr[j++] = str[i];
                cnt = 1;
            }
        }
    }
    compressedReturnStr[j] = '\0';
    return compressedReturnStr;
}
//Possible tips if you want to save the appearance with decimal numbers:
//char snum[5];
//itoa(cnt, snum, 10);
//char result[2];
//sprintf(result, "%d", (cnt));