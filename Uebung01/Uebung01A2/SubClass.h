//
//  SubClass.h
//  C++
//

#ifndef __C____SubClass__
#define __C____SubClass__

#include "SuperClass.h"

class SubClass : public SuperClass {
private:
    int c;
    
public:
    SubClass(int vA, int vB, int vC);
    ~SubClass();
   
    void print(void);
    virtual void printVirtual(void);
};

#endif /* defined(__C____SubClass__) */
