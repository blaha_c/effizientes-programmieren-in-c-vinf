//
//  SuperClass.h
//  C++
//

#ifndef __C____SuperClass__
#define __C____SuperClass__

class SuperClass {
 private:
  int a,b;
  
 public:
  SuperClass(int vA, int vB);
  ~SuperClass();
  
  void print(void);
  void setA(int a);
  virtual void printVirtual(void);
};

#endif /* defined(__C____SuperClass__) */
