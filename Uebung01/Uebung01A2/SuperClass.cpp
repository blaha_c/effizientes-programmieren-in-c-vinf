//
//  SuperClass.cpp
//  C++
//

#include <iostream>

#include "SuperClass.h"

SuperClass::SuperClass(int vA, int vB) : a(vA), b(vB)
{
    std::cout << "SuperClass(a,b)" << std::endl;
}

SuperClass::~SuperClass()
{
    std::cout << "~SuperClass(a,b)" << std::endl;
}

void SuperClass::setA(int a)
{
    this->a = a;
}
void SuperClass::print(void)
{
    std::cout << "SuperClass::Print " << a << " | " << b << std::endl;
}

void SuperClass::printVirtual(void)
{
    std::cout << "SuperClass::PrintVirtual " << a << " | " << b << std::endl;
}
