//
//  SubClass.cpp
//  C++
//
#include <iostream>

#include "SubClass.h"

SubClass::SubClass(int vA, int vB, int vC) : SuperClass(vA, vB), c(vC)
{
    std::cout << "SubClass(int vA, int vB, int vC)" << std::endl;
}

SubClass::~SubClass()
{
    std::cout << "~SubClass()" << std::endl;
}


void SubClass::print(void)
{
    std::cout << "SubClass::print() " << c << std::endl;
}


void SubClass::printVirtual(void)
{
    std::cout << "SubClass::printVirtual() " << c << std::endl;
}
