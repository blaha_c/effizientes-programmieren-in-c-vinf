#include <iostream>

#include "SuperClass.h"
#include "SubClass.h"

int main(int argc, const char * argv[])
{

    SuperClass *super, *sub, *test;

    super = new SuperClass(1,2);
    sub   = new SubClass(3,4,5);
    
    std::cout << "Start" << std::endl;
    
    test = super;
    
    super->print();
    super->printVirtual();
    std::cout << std::endl;
    
    sub->print();
    sub->printVirtual();
    std::cout << std::endl;

    test->setA(42);
    
    super->print();
    super->printVirtual();
    std::cout << std::endl;
    
    delete super;
    delete sub;
    
    return 0;
}
