//
// Created by ArbeitsPC on 10.05.2021.
//
#include "doSort.h"
#include <stdio.h>

static void swap(int *a, int *b){
    int zwsp;
    zwsp = *a;
    *a = *b;
    *b = zwsp;
}

void doSort(int aField[], int cnt){
    for (int i=0; i<cnt; ++i){
        for (int j=1; j<cnt-i; ++j){
            if (aField[j-1] > aField[j])
                swap(&aField[j-1],&aField[j]);
        }
    }
}

void printOut(int a[], int cnt){
    doSort(a, cnt);
    int count = 1;
    for (int j=1; j<cnt+1; ++j){
        if (a[j-1] == a[j])
            ++count;
        if (a[j-1] != a[j]) {
            printf("%3d   %3d\n", a[j-1], count);
            count = 1;
        }
    }
}