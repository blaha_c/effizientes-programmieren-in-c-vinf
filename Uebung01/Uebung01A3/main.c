//
// Created by ArbeitsPC on 10.05.2021.
//
#include <stdio.h>
#include "doSort.h"

int main(int argc, const char * argv[]){
    //int data[] = {34, 32, 12, 55, 67, 2, 42, 21};
    int data[] = {7, 2, 2, 4, 2, 5, 1, 7};
    int cnt = sizeof(data)/sizeof(int);

    for (int i=0; i<cnt; ++i)
       printf("%3d ", data[i]);
    printf("\n");

    doSort(data, cnt);

    for (int i=0; i<cnt; ++i)
       printf("%3d ", data[i]);
    printf("\n");

    printOut(data, cnt);

    return 0;
}