//
// Created by ArbeitsPC on 17.06.2021.
//
#include "Uebung06A1.h"
#include <stdio.h>

int main() {
    //Part 1
    /*
    int a = countYZ("fez day"); //→ 2
    int b = countYZ("day fez"); //→ 2
    int c = countYZ("day fyyyz"); //→ 2
    printf("%2d, %2d, %2d\n", a, b, c);
    */

    //Part 2
    /*
    char *d = mirrorEnds("abXYZba"); //→ "ab"
    char *e = mirrorEnds("abca"); //→ "a"
    char *f = mirrorEnds("aba"); //→ "aba"
    printf("%s, %s, %s\n", d, e, f);
    */

    //Part 3
    /*
    char *h = notReplace("is test"); //→ "is not test"
    char *i = notReplace("is-is"); //→ "is not-is not"
    char *j = notReplace("This is right"); //→ "This is not right"
    char *k = notReplace("Is This is right, but this is right too and this is really right, is");
    printf("%s\n%s\n%s\n%s\n\n", h, i, j, k);
    */

    //Part 3: Stöhr Peter
    char *str = "is test";
    printf("%s --> %s\n", str, notReplace(str));

    str = "is-is";
    printf("%s --> %s\n", str, notReplace(str));

    str = "This is right";
    printf("%s --> %s\n", str, notReplace(str));

    //Part 4
    /*
    char *k = zipZap("zipXzap"); //→ "zpXzp"
    char *l = zipZap("zopzop"); //→ "zpzp"
    char *m = zipZap("zzzopzop"); //→ "zzzpzp"
    char *n = zipZap("zzpp"); //→ "zp"
    printf("%s\n%s\n%s\n%s\n", k, l, m, n);
    */
    return 0;
}