//
// Created by ArbeitsPC on 17.06.2021.
//
#ifndef EFFIZIENTES_PROGRAMMIEREN_IN_C_VINF_UEBUNG06A1_H
#define EFFIZIENTES_PROGRAMMIEREN_IN_C_VINF_UEBUNG06A1_H

extern int countYZ(char str[]);
extern char *mirrorEnds(char string[]);
extern char *notReplace(char str[]);
extern char *notReplaceStoehr(char *origin);
extern char *zipZap(char str[]);

#endif //EFFIZIENTES_PROGRAMMIEREN_IN_C_VINF_UEBUNG06A1_H