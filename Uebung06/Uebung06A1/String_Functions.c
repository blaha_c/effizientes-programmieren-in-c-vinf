//
// Created by ArbeitsPC on 17.06.2021.
//
#include "Uebung06A1.h"
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
//Stöhr Peter's solution
#include <stdio.h>

//Part 1: https://codingbat.com/prob/p199171
int countYZ(char str[]) {
    char previousChar = str[0];
    int i = 1;
    int sum = 0;
    do {
        if(previousChar == 'y' || previousChar == 'z'){
            if(str[i] == ' ' || str[i] == '\0'){
                ++sum;
            }
        }
        previousChar = str[i];
        i++;
    } while(str[i-1] != '\0');
    return sum;
}

//Part 2: https://codingbat.com/prob/p139411
char *mirrorEnds(char string[]) {
    size_t len = strlen(string);
    char *returnString = (char *)malloc(sizeof(char));
    returnString[0] = '\0';
    if(len == 1){
        return string;
    }
    for(int i = 0; i < len/2; i++){
        if(string[i] == string[len - i - 1]){
            if (i == len/2 - 1) {
                if(len % 2 == 1){
                    //Here you could also write "return string".
                    returnString = string;
                    break;
                }
                returnString[i] = string[i];
                returnString = (char *)realloc(returnString, sizeof(char));
                returnString[i+1] = '\0';
                break;
            }
            returnString[i] = string[i];
            returnString = (char *)realloc(returnString, sizeof(char));
        }else{
            returnString[i] = '\0';
            break;
        }
    }
    return returnString;
}

//Part 3: https://codingbat.com/prob/p154137
char *notReplace(char str[]) {
    size_t len = strlen(str);

    char *iterationStr = (char *) malloc((len + 3) * sizeof(char));
    char *subStr = (char *) malloc(1 * sizeof(char));
    char pastedString[] = " not";
    char *returnStr = (char *) malloc((len + 1) * sizeof(char));

    iterationStr[0] = ' ';
    iterationStr[1] = '\0';
    strcat(iterationStr, str);
    iterationStr[len + 1] = ' ';
    iterationStr[len + 2] = '\0';
    //Following line is needed due to strcat()
    returnStr[0] = '\0';

    size_t lenPasted = strlen(pastedString);
    int appearance = 0;
    int previousApIndex = 0;

    for (int i = 0; i < len - 1; i++) {
        if (isalpha(iterationStr[i]) == 0) {
            if (iterationStr[i + 1] == 'i' || iterationStr[i + 1] == 'I') {
                if (iterationStr[i + 2] == 's') {
                    if (isalpha(iterationStr[i + 3]) == 0) {
                        ++appearance;
                        if(appearance == 1){
                            subStr = (char *)realloc(subStr, (i + 2) * sizeof(char));
                            strncpy(subStr, str, i + 2);
                            //Following line is inspired by: https://www.cplusplus.com/reference/cstring/strncpy/
                            subStr[i + 2] = '\0';
                        }else{
                            subStr = (char *)realloc(subStr, (i - previousApIndex) * sizeof(char));
                            strncpy(subStr, (str + previousApIndex + 2), i - previousApIndex);
                            subStr[i - previousApIndex] = '\0';
                        }
                        strcat(returnStr, subStr);
                        returnStr = (char *)realloc(returnStr, (appearance * lenPasted + len + 2) * sizeof(char));
                        strcat(returnStr, pastedString);
                        previousApIndex = i;
                    }
                }
            }
        }
    }
    //Copies the rest of the string
    subStr = (char *)realloc(subStr, (len - previousApIndex) * sizeof(char));
    strncpy(subStr, str + previousApIndex + 2, len - previousApIndex);
    strcat(returnStr, subStr);
    free(iterationStr);
    free(subStr);
    return returnStr;
}

//Part 3: https://codingbat.com/prob/p154137
//Stöhr Peter's solution
//
// Created by Peter Stöhr on 18.06.21.
//
char *notReplaceStoehr(char *origin){
    char *toBeAdded = " not";

    if (strlen(origin) < 2)
        return origin;

    char *result = strdup(origin);
    size_t resLength = strlen(origin);

    char *workingStr = malloc((strlen(origin)+1+2)*sizeof(char));
    sprintf(workingStr, " %s ", origin);

    int resPos = 2;
    for (int pos=3; pos<strlen(workingStr); pos++){
        if ((!isalnum(workingStr[pos-3])) && (workingStr[pos-2] == 'i') &&
        (workingStr[pos-1] == 's') && (!isalnum(workingStr[pos]))){
            resLength += strlen(toBeAdded);
            result = realloc(result, resLength*sizeof(char));
            for (int i=0; i<strlen(toBeAdded); i++)
                result[resPos++] = toBeAdded[i];
        }
        result[resPos++] = workingStr[pos];
    }
    result[resLength] = '\0';

    free(workingStr);
    return result;
}

//Part 4: https://codingbat.com/prob/p180759
char *zipZap(char str[]) {
    size_t len = strlen(str);
    char *str1 = (char *)malloc((len + 1) * sizeof(char)); //It doesn't really matter how big the storage area is...
    str1 = str;
    char *returnString = (char *)malloc(sizeof(char));
    int apIndex = -2;
    int appearance = 0;
    for (int i = 0; i < len; i++){
        if(str1[i] == 'z'){
            if(str[i + 2] == 'p' && i < len - 2){
                for(int j = apIndex + 2; j < i + 1; j++){
                    returnString[j - appearance] = str1[j];
                    returnString = (char *)realloc(returnString, sizeof(char));
                }
                ++appearance;
                apIndex = i;
            }
        }
    }
    //Copies the rest of the string
    for(int i = apIndex + 2; i < len; i++){
        returnString[i - appearance] = str1[i];
        returnString = (char *)realloc(returnString, sizeof(char));
    }
    returnString[len - appearance] = '\0';

    return returnString;
}