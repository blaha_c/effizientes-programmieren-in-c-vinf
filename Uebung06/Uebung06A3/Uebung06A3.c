//
// Created by ArbeitsPC on 20.06.2021.
//
/* Disclaimer: ssize_t getline (char **lineptr, size_t *n, FILE *stream)
 * https://openbook.rheinwerk-verlag.de/c_von_a_bis_z/016_c_ein_ausgabe_funktionen_016.htm
 *
 * >>
 * Indeed, getline was never part of the C standard library, but is a Unix/POSIX extension.
 * To get access to it, you need to compile with gcc in POSIX ("gnu") mode under Unix,
 * in which case the compiler drops the function inside stdio.h.
 * And dropping non-standard functions inside standard headers is non-compliant behavior.
 *
 * To get this working under Windows, you'd need to use Cygwin to emulate Unix under Windows.
 * The gcc/mingw port won't do, since that one uses Microsoft's C standard lib.
 *
 * The good news is that you probably don't want to use getline anyway (even in POSIX),
 * since that function has a horrible API and is a major source for memory leaks in buggy C programs.
 * It's one of those strange, ancient Unix functions that simply should be avoided.
 * Instead consider using fgets with a caller-allocated buffer: faster, safer, portable standard C.
 * <<
 * Resource: https://stackoverflow.com/questions/66205441/getline-function-not-compiling-with-mingw-w64-gcc
 *
 * -> Instead usage of gets() or fgets().
 */
#include <stdio.h>
#include <stdlib.h>
#define LINELENGTH 1000

int main(){
    FILE *fp;
    char buffer[LINELENGTH];

    /* opening file for reading */
    fp = fopen("Z:\\IDE-Workspace\\CLion\\effizientes-programmieren-in-c-vinf\\Uebung06\\Uebung06A3\\Textdatei.txt", "r");
    if(fp == NULL) {
        perror("Error opening file");
        return EXIT_FAILURE;
    }
    while (fgets(buffer, sizeof(buffer), fp)) {
        printf("%s", buffer);
    }
    fclose(fp);
    return EXIT_SUCCESS;
}