//
// Created by ArbeitsPC on 18.06.2021.
//
#include "Uebung06A2.h"
#include <stdbool.h>
#include <stdio.h>

int main() {
    bool a = isHappy(49);
    bool b = isHappy(42);

    //False: printf("%s\t%s", a, b);
    printf("%d\t%d", a, b);

    return 0;
}