//
// Created by ArbeitsPC on 18.06.2021.
//
#include "Uebung06A2.h"
#include <stdbool.h>

int squareOfDigits(int num) {
    int remainder;
    int sum = 0;
    int temp = num;
    while(temp > 0){
        remainder = temp % 10;
        sum += remainder * remainder;
        temp /= 10;
    }
    return sum;
}

bool isHappy(int num){
    if(num == 4){
        return false;
    }else if (num == 1){
        return true;
    }
    int a = squareOfDigits(num);
    for(;;){
        a = squareOfDigits(a);
        if( a == 4){
            return false;
        }else if (a == 1){
            return true;
        }
    }
}