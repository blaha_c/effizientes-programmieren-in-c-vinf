//
// Created by ArbeitsPC on 22.06.2021.
//
#include "Uebung06C1.h"
#include <stdio.h>
/*
 * Disclaimer: Works only due to a global variable: sumAB
 */
int sumAB[] = {0, 0};

int findMax(int A[], int n, int lenth){
    int len = lenth;
    if (A[len - 1] < A[n - 2]) {
        int cache = A[len - 1];
        A[len - 1] = A[n - 2];
        A[n - 2] = cache;
    }
    //Shorthand "?:" can't contain a return-statement
    if(n == 2) {
        return A[len - 1];
    }else {
        findMax(A, n - 1, len);
    }
}

void addMax(int max){
    (sumAB[1] < sumAB[0]) ? (sumAB[1] += max) : (sumAB[0] += max);
}

int greenRedArray(int *A, int n){
    if(n == 0) {
        if(sumAB[0] == sumAB[1]) {
            sumAB[0] = 0;
            sumAB[1] = 0;
            return 1;
        } else {
            sumAB[0] = 0;
            sumAB[1] = 0;
            return 0;
        }
    }else{
        if(n > 1){
            int max = findMax(A, n, n);
            addMax(max);
        }else if(n == 1){
            int max = A[0];
            addMax(max);
        }
        greenRedArray(A, n - 1);
    }
}