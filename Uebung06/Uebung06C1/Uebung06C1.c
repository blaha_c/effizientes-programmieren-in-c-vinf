//
// Created by ArbeitsPC on 22.06.2021.
//
#include "Uebung06C1.h"
#include <stdio.h>
int main(){
    int a[] = {2, 1, 2};
    int b[] = {1, 1, 1, 5, 1, 1};
    int c[] = {1, 1, 2, 1, 3, 1, 1};
    int d[] = {1, 1, 5, 1, 1};
    printf("%d \n", greenRedArray(a, sizeof(a)/sizeof(a[0])));
    printf("%d \n", greenRedArray(b, sizeof(b)/sizeof(b[0])));
    printf("%d \n", greenRedArray(c, sizeof(c)/sizeof(c[0])));
    printf("%d \n", greenRedArray(d, sizeof(d)/sizeof(d[0])));
    return 0;
}