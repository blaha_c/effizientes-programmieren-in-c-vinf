//
// Created by ArbeitsPC on 11.05.2021.
//
#ifndef EFFIZIENTES_PROGRAMMIEREN_IN_C_VINF_UEBUNG02A2_H
#define EFFIZIENTES_PROGRAMMIEREN_IN_C_VINF_UEBUNG02A2_H

extern int * SieveOfEratosthenes(int n);
//Die Länge des Arrays muss zusätzlich verwaltet werden.
//Ich mache dies in der globalen Variable length. (schlechte Lösung)
//Besser wäre z. B. ein Struct mit Array und length-Variable.
int length;

#endif //EFFIZIENTES_PROGRAMMIEREN_IN_C_VINF_UEBUNG02A2_H