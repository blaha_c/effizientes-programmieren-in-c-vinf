//
// Created by ArbeitsPC on 11.05.2021.
//
#include <stdlib.h>
#include "Uebung02A2.h"

int * SieveOfEratosthenes(int n){

//Create Array
    --n; //Removing of 1, becaus it's no prime number.
    //It is fine style to cast the return of malloc() -> (void *) to the target data type.
    int * numbers = (int *)malloc(sizeof(int) * n);
    for(int i=2; i<n+2; i++) {
        numbers[i-2] = i;
    }

//Calculation prime numbers
    //Inspired by: https://www.geeksforgeeks.org/delete-an-element-from-array-using-two-traversals-and-one-traversal/
    int indexPrimeNumber = 0;
    int i = 1; //numbers[0]->(2) soll nicht überprüft werden.
    while(indexPrimeNumber < n){
        while (i < n) {
            if (numbers[i] % numbers[indexPrimeNumber] == 0) {

//Deleting & Move up
                n = n - 1;
                for (int j=i; j<n; j++) {
                    numbers[j] = numbers[j + 1];
                }
                --i;
            }
            ++i;
        }
        ++indexPrimeNumber;
        i = indexPrimeNumber + 1;
    }

    length = n;
    //C can't return arrays. Workarounds:
    //Resource: https://stackoverflow.com/questions/8108634/global-variables-in-header-file/8109149
    //1. A struct containing an array
    //2. A static array
    //3. Obtain storage with malloc()
    return numbers;
}