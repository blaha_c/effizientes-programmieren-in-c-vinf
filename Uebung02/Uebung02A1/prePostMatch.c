//
// Created by ArbeitsPC on 11.05.2021.
//
#include "Uebung02A1.h"
#include <stdbool.h>

static bool equals(int StartInS1, int StartInS2, int checkLength, int aArray[]){
    for(int i = StartInS1; i < StartInS1 + checkLength; i++, StartInS2++){
        if(aArray[i] != aArray[StartInS2]){
            return false;
        }
    }
    return true;
}

int prePostMatch(int aArray[], int cnt){
    if(cnt < 4) {
        //No sequence can occur twice in a 3-element array.
        return 0;
    }
    int minStringLength = 2;
    //Reduction of the length (checkLength) of the substrings in the array.
    for(int checkLength = cnt/2; checkLength >= minStringLength; checkLength--){
        //Possible start indices within the array of String1
        for(int StartInS1 = 0; StartInS1 + checkLength <= cnt - checkLength; StartInS1++){
            //Possible start indices within the array of String2
            for(int StartInS2 = StartInS1 + checkLength; StartInS2 + checkLength <= cnt; StartInS2++){
                if(equals(StartInS1, StartInS2, checkLength, aArray) == true){
                    return checkLength;
                }
            }
        }
    }
    return 0;
}