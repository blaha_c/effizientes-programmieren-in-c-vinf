//
// Created by ArbeitsPC on 11.05.2021.
//
#include <stdio.h>
#include "Uebung02A1.h"

int main() {
    int data1[] = {42, 21, 7, 5, 42, 21, 7};
    int data2[] = {42, 21, 7, 5, 5, 21};
    int data3[] = {42, 21, 3, 4, 5, 6, 7, 42, 21};

    int cnt1 = sizeof(data1)/sizeof(int);
    int cnt2 = sizeof(data2)/sizeof(int);
    int cnt3 = sizeof(data3)/sizeof(int);

    printf("%3d\n", prePostMatch(data1, cnt1));
    printf("%3d\n", prePostMatch(data2, cnt2));
    printf("%3d\n", prePostMatch(data3, cnt3));

    return 0;
}