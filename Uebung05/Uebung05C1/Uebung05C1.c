//
// Created by ArbeitsPC on 16.06.2021.
//
#include <stdio.h>
//Aim: Change only 1 character in the code to produce only 5 character in the output
/*
    int main(){
    int i;
    int n = 5;
    for(i=0; i<n; i-- )
        printf("-");
    return 0;
}
*/

//Solution 1: '-' -> '+'
int main(){
    int i;
    int n = 5;
    for(i=0; i<n; i++ )
        printf("+");
    return 0;
}