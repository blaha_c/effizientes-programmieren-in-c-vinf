//
// Created by ArbeitsPC on 16.06.2021.
//
#include "Uebung05A3.h"
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

//Exercise a)
//Read: http://www.cplusplus.com/reference/cstdarg/va_start/

//Exercise b)
char* strcatAll(char *s1, ...){
    char* concatenatedString = (char*)malloc(100 * sizeof(char));

    //resource: https://stackoverflow.com/questions/59533719/strcat-adds-junk-to-the-string
    //Without this line we get 3 wrong characters in the beginning of the String
    concatenatedString[0] = '\0';

    //Inspired by: http://www.cplusplus.com/reference/cstdarg/va_end/
    va_list vl;
    va_start(vl, s1);
    do {
        strcat(concatenatedString, s1);
        s1 = va_arg(vl,char*);
    } while (s1 != NULL);
    va_end(vl);

    return concatenatedString;
}