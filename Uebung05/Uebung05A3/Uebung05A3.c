//
// Created by ArbeitsPC on 16.06.2021.
//
#include "Uebung05A3.h"
#include <stdio.h>

int main() {
    char *a = strcatAll("Oster", "hasen", "korb", NULL);
    char *b = strcatAll("Hasen", "nase", NULL);
    char *c = strcatAll("Ende", NULL);
    printf("%s\n", a);
    printf("%s\n", b);
    printf("%s\n", c);

    return 0;
}