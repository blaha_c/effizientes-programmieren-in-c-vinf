//
// Created by ArbeitsPC on 16.06.2021.
//
#include <stdio.h>
#include <string.h>

int main() {
    char str[80];
    char *sep = ".,";
    char *part;
    strcpy(str, "Ra.bimmel, Ra.bammel, Ra.bumm");
    part = strtok(str, sep);
    while (part != NULL){
        printf("%s\n", part);
        part = strtok(NULL, sep);
    }

    return 0;
}