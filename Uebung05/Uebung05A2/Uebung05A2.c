//
// Created by ArbeitsPC on 16.06.2021.
//
#include "Uebung05A2.h"
#include <stdio.h>

int main() {
    int aArray[] = {-12, 23, 20, 20, 32, 56, 5, -12, 1, 1, 1};
    int cnt = sizeof(aArray)/sizeof(int);
    int * minMax = findMinMax(aArray, cnt);
    printf("Min:%3d; Max:%3d\n", minMax[0], minMax[1]);

    return 0;
}