//
// Created by ArbeitsPC on 16.06.2021.
//
#include "Uebung05A2.h"
#include <stdlib.h>
//Exercise a)
//Type signature: int * findMinMax(int * aArray, int cnt)

//Exercise b)
//count comparisons = ~1,5 * n
int * findMinMax(int * aArray, int cnt){
    int * minMax = (int *)malloc(2 * sizeof(int));
    int smallest = aArray[0];
    int biggest = aArray[0];

    for (int i = 1; i < cnt; i++) {
        if (aArray[i] < smallest) {
            smallest = aArray[i];
        }else if (aArray[i] > biggest) {
            biggest = aArray[i];
        }
    }

    minMax[0] = smallest;
    minMax[1] = biggest;
    return minMax;
}