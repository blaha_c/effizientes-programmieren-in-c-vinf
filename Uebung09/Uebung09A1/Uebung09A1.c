//
// Created by ArbeitsPC on 24.06.2021.
//
// Subtask 1:
/*
#include <stdio.h>
int a[3][3] = { {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9} };
int *pa[3] = {a[0], a[1], a[2]};
int *p=a[0];
int main(int argc, const char * argv[]){
    for (int i=0; i<3; i++)
        printf("%d %d %d\n", a[i][2-i], *a[i], *(*(a+i)+i));
    for (int i=0; i<3; i++)
        printf("%d %d\n", *pa[i], p[i]);
    return 0;
}
*/

//Always think about:
//a[i][j] = *(*(a + i)+ j) and first [] then *.
//3 1 1
//5 4 5
//7 7 9
//1 1
//4 2
//7 3

// Subtask 2:
#include <stdio.h>
#include <stdlib.h>
char *c[] = {
        "Enter",
        "New",
        "Point",
        "First"
};
char **cp[] = {c+3, c+2, c+1, c};
char ***cpp = cp;
int main(int argc, char *argv[]) {
    printf("%s", **++cpp);
    printf("%s ", *--*++cpp+3);
    printf("%s", *cpp[-2]+3);
    printf("%s\n", cpp[-1][-1]+1);
    return EXIT_SUCCESS;
}

//Always think about:
//It is a 3D Array with the size of cpp[1][4][4-6]
//a[i][j][k] = *(*(a + i)+ j) + k and first [] then *.
//Point     ->cpp[0][1][0]  -> until '\0'
//er        ->cpp[0][j][k]  -> I don't come to the right answer.
//st        ->cpp[0][j][3]  -> I don't come to the right answer.
//ew        ->cpp[-1][-1][1]-> I don't come to the right answer.