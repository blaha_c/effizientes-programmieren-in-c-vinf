//
// Created by ArbeitsPC on 30.06.2021.
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Resource: https://www.cplusplus.com/reference/cstdlib/qsort/?kw=qsort
int compare(const void *a, const void *b){
    //The "trick" is to set the required * inside the cast (const char**) and not outside the cast, I tried.
    //Resource: https://www.geeksforgeeks.org/c-program-sort-array-names-strings/
    return strcmp(*(const char**)a, *(const char**)b);
}

int main(void){
    char *vStr[] = {"Linus", "Lucy", "Schroeder", "Snoopy", "Woodstock",
                    "Charlie Brown", "Sally", "Peppermint Patty", "Marcy", "Franklin"};

    int vStrCnt = (sizeof(vStr)/sizeof(vStr[0]));

    qsort(vStr, vStrCnt, sizeof(const char*), compare);

    for (int i=0; i<vStrCnt; i++)
        printf("%s\n", vStr[i]);

    return 0;
}