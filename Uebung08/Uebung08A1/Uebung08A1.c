//
// Created by ArbeitsPC on 24.06.2021.
//
#include "Uebung08A1.h"
#include <stdio.h>

// Original string:     "~ao~~aao~~~aaao~~~~aaaao~~~~~aaaaao"
// Compressed string:   "~~ao~ ~aao~!~aaao~"~~"ao~#~~#ao"
// For '"' we have to insert '\' before the quotation marks.
// Adapted String:      "~~ao~ ~aao~!~aaao~\"~~\"ao~#~~#ao"

int main() {
    char string[] = "~~ao~ ~aao~!~aaao~\"~~\"ao~#~~#ao";
    char *stringKom = redoRLC(string);

    puts(stringKom);

    return 0;
}