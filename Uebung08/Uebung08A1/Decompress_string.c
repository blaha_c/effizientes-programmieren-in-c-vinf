//
// Created by ArbeitsPC on 24.06.2021.
//
#include "Uebung08A1.h"
#include <stdlib.h>

char *redoRLC(char *str){
    char *decompressedStr = (char *)malloc(sizeof(char));
    int j = 0;

    for (int i = 0; str[i] != '\0'; i++) {
        if(str[i] == '~' && str[i + 1] == '~'){
            decompressedStr = (char *)realloc(decompressedStr, (i + 2) * sizeof(char));
            decompressedStr[j++] = str[i++];
        }else if(str[i] == '~'){
            int a = (int)(str[i + 1] - ' ' + 2);
            decompressedStr = (char *)realloc(decompressedStr, (i + 2 + a) * sizeof(char));
            for(int k = 0; k < a; k++){
                decompressedStr[j++] = str[i + 2];
            }
            i += 2;
        }else{
            decompressedStr = (char *)realloc(decompressedStr, (i + 2) * sizeof(char));
            decompressedStr[j++] = str[i];
        }
    }

    decompressedStr[j] = '\0';
    return decompressedStr;
}