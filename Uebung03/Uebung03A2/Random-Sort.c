//
// Created by ArbeitsPC on 12.05.2021.
//
#include "Uebung03A2.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
//timestamp()
#include <stdint.h>
//microsecond()
#include <libiberty.h>
//nanosecond()
#include <inttypes.h>

//sorted() muss vor randomSort, da nicht im Header (.h) eingetragen
//static muss nicht unbedingt sein.
static bool sorted(int aArray[], int cnt){
    for(int i=0; i<cnt-1; i++){
        if(aArray[i] > aArray[i+1]){
            return false;
        }
    }
    return true;
}

//Erklärung des Array-Shuffle:
//Tauschen des letzen Elements aArray[4] mit einem Element aus aArray[0-4]
//Tauschen des vorletzten Element aArray[3] mit einem Element aus aArray[0-3] usw.
void randomSort(int aArray[], int cnt) {
    int temp, randomNumber;
    time_t t;
    srand((unsigned) time(&t));                 //Dazu da, dass das Random bei jedem Kompiliervorgang anders ist.
    for(;;) {                                   //Endlosschleife (The C Programming Language by Kernighan and Ritchie)
        for (int i=cnt-1; i>0; i--) {
            temp = aArray[i];
            randomNumber = (rand() % (i+1));    //+1, damit der Wert auch am gleichen Arrayplatz bleiben könnte.
            //printf("Zufallszahl: %1d\n", randomNumber);
            aArray[i] = aArray[randomNumber];
            aArray[randomNumber] = temp;
        }
        //Ausgabe des geshuffelten Arrays
        /*
        printf("Array: ");
        for (int i = 0; i < cnt; ++i)
            printf("%1d ", aArray[i]);
        printf("\n");
        */
        if (sorted(aArray, cnt) == true) {
            break;
        }
    }
}

//Random ist bei jedem Kompiliervorgang gleich.
void random1(){
    for(int i=0; i<10; i++) {
        printf("%2d", rand() % 6);
    }
    printf("\n");
}

//Random ist bei jedem Kompiliervorgang anders.
//Zum mehrfachen Setzen des Randoms innerhalb eines Kompiliervorgans ungeeignet, da bei direkt aufeinderfolgenden
//Abfragen ungefähr nur alle 232 ein neuer/anderer timestamp zurückkommt. (egal ob micro- oder nanoseconds)
void random2(){
    time_t t ;
    srand((unsigned) time(&t));
    for(int i=0; i<10; i++) {
        printf("%2d", rand() % 6);
    }
    printf("\n");
}

//Quelle: https://en.cppreference.com/w/c/chrono/time_t
void timestamp(){
    time_t now;
    time(&now);
    printf("%d Sekunden seit 01.01.1970 00:00:00 Uhr\n", now);
    printf("%jd Sekunden seit 01.01.1970 00:00:00 Uhr\n", (intmax_t)now);
    printf("%s", asctime(gmtime(&now))); //Universal Time (UTC)/GMT timezone
}

//Quelle: https://stackoverflow.com/questions/5833094/get-a-timestamp-in-c-in-microseconds
void microsecond(){     //micro [µ] = 10^(-6) = 0.000 001
    struct timeval tv;
    gettimeofday(&tv, NULL);
    unsigned long time_in_micros = /*1000000 * tv.tv_sec +*/tv.tv_usec;
    printf("%lu\n", time_in_micros);
}

//Quelle: https://stackoverflow.com/questions/39439268/printing-time-since-epoch-in-nanoseconds
void nanosecond() {     //nano [n] = 10^(-9) = 0.000 000 001
    long int ns;
    uint64_t all;
    //time_t sec;
    struct timespec spec;

    clock_gettime(CLOCK_REALTIME, &spec);
    //sec = spec.tv_sec;
    ns = spec.tv_nsec;
    all = /*(uint64_t)sec * 1000000000L +*/ (uint64_t) ns;

    printf("%" PRIu64  "\n", all);
}