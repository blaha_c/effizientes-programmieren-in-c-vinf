//
// Created by ArbeitsPC on 12.05.2021.
//
#include "Uebung03A2.h"
#include <stdio.h>

int main() {
    int aArray[] = {1, 2, 3, 4, 5, 6};
    int cnt = sizeof(aArray)/sizeof(int);
    randomSort(aArray, cnt);

//Zum testen von randomSort() ohne sorted() & for(;;)
/*
    for (int i=0; i<20; i++) {
        randomSort(aArray, cnt);
        //Ausgabe Array
        for (int i = 0; i < cnt; ++i)
            printf("%1d ", aArray[i]);
        printf("\n");
        //Zurücksetzen zu {1, 2, 3, 4, 5, 6}
        for (int i=0; i<6; i++)
            aArray[i] = i+1;
    }
*/

//Zum Testen bei direkt aufeinanderfolgenden Aufrufe einer Funktion
/*
    for(int i=0; i<5000; i++) {
        //random1();
        //random2();
        //timestamp();
        //microsecond();
        //nanosecond();
    }
*/
    return 0;
}