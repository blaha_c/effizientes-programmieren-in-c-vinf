//
// Created by ArbeitsPC on 12.05.2021.
//
#ifndef EFFIZIENTES_PROGRAMMIEREN_IN_C_VINF_UEBUNG03A2_H
#define EFFIZIENTES_PROGRAMMIEREN_IN_C_VINF_UEBUNG03A2_H

extern void randomSort(int aArray[], int cnt);
extern void random1();
extern void random2();
extern void timestamp();
extern void microsecond();
extern void nanosecond();

#endif //EFFIZIENTES_PROGRAMMIEREN_IN_C_VINF_UEBUNG03A2_H