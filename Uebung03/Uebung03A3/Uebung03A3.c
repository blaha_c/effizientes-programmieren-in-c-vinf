//
// Created by ArbeitsPC on 13.05.2021.
//
#include "Uebung03A3.h"
#include<stdio.h>
int main() {
    //!a) Datentypen
    /*
    int intType;
    float floatType;
    double doubleType;
    char charType;
    short shortType;
    long longType;

    printf("Size of int: %zu bytes\n", sizeof(intType));
    printf("Size of float: %zu bytes\n", sizeof(floatType));
    printf("Size of double: %zu bytes\n", sizeof(doubleType));
    printf("Size of char: %zu byte\n", sizeof(charType));
    printf("Size of short: %zu byte\n", sizeof(shortType));
    printf("Size of long: %zu byte\n", sizeof(longType));
    */

    //!b) Kombination Datentypen
    /*
    //Die Reihenfolge spielt normal keine Rolle.
//----------UNSIGNED------------------------------------------------------------
    //valid
    unsigned int uintType;
    unsigned char ucharType;
    unsigned short ushortType;
    unsigned long ulongType;
    //invalid
    //unsigned float ufloatType;
    //unsigned double udoubleType;
    printf("Size of unsigned int: %zu bytes\n", sizeof(uintType));
    printf("Size of unsigned char: %zu byte\n", sizeof(ucharType));
    printf("Size of unsigned short: %zu byte\n", sizeof(ushortType));
    printf("Size of unsigned long: %zu byte\n", sizeof(ulongType));

//----------SHORT---------------------------------------------------------------
    //valid
    short int sintType;
    //invalid
    //short float floatType;
    //short double doubleType;
    //short char charType;
    //short short sshortType;
    //short long longType;
    printf("Size of short int: %zu bytes\n", sizeof(sintType));

//----------LONG----------------------------------------------------------------
    //valid
    long int lintType;
    long double ldoubleType;
    long long llongType;
    //invalid
    //long float floatType;
    //long char charType;
    //long short shortType;
    printf("Size of long int: %zu bytes\n", sizeof(lintType));
    printf("Size of long double: %zu bytes\n", sizeof(ldoubleType));
    printf("Size of long long: %zu byte\n", sizeof(llongType));

//----------UNSIGNED-SHORT------------------------------------------------------
    //valid
    unsigned short int usintType;
    //invalid
    //unsigned short float usfloatType;
    //unsigned short double usdoubleType;
    //unsigned short char uscharType;
    //unsigned short short usshortType;
    //unsigned short long uslongType;
    printf("Size of unsigned short int: %zu bytes\n", sizeof(usintType));

//----------UNSIGNED-LONG-------------------------------------------------------
    //valid
    unsigned long int ulintType;
    unsigned long long ullongType;
    //invalid
    //unsigned long float floatType;
    //unsigned long double doubleType;
    //unsigned long char charType;
    //unsigned long short shortType;
    printf("Size of unsigned long int: %zu bytes\n", sizeof(ulintType));
    printf("Size of unsigned long long: %zu byte\n", sizeof(ullongType));

//-----------SHORT-LONG---------------------------------------------------------
    //invalid
    //short long int slintType;
    //short long float slfloatType;
    //short long double sldoubleType;
    //short long char slcharType;
    //short long short slshortType;
    //short long long sllongType;

//-----------UNSIGNED-SHORT-LONG------------------------------------------------
    //invalid
    //unsigned short long int slintType;
    //unsigned short long float slfloatType;
    //unsigned short long double sldoubleType;
    //unsigned short long char slcharType;
    //unsigned short long short slshortType;
    //unsigned short long long sllongType;
    */

    //!c) Wertebereiche Datentypen
    //Gefundene limits.h:
    //D:\mingw-w64-03-20-2021\include\c++\4.8.3\tr1                                     ->No help
    //D:\mingw-w64-03-20-2021\lib64\gcc\x86_64-w64-mingw32\4.8.3\install-tools\include  ->Partly helpful
    //D:\mingw-w64-03-20-2021\lib64\gcc\x86_64-w64-mingw32\4.8.3\include-fixed          ->Partly helpful
    //D:\mingw-w64-03-20-2021\mingw\include                                             ->helpful
    //D:\mingw-w64-03-20-2021\x86_64-w64-mingw32\include                                ->helpful

    printf("NORMAL----------------------\n");
    printf("int Range                => ");
    SignedRange(BITS(int));
    printf("float Range              => ");
    SignedRange(BITS(float));
    printf("double Range             => "); //k. A.
    SignedRange(BITS(double));
    printf("char Range               => ");
    SignedRange(BITS(char));
    printf("short Range              => ");
    SignedRange(BITS(short));
    printf("long Range               => ");
    SignedRange(BITS(long));

    printf("UNSIGNED--------------------\n");
    printf("unsigned int Range       => ");
    UnsignedRange(BITS(unsigned int));
    printf("unsigned char Range      => ");
    UnsignedRange(BITS(unsigned char));
    printf("unsigned short Range     => ");
    UnsignedRange(BITS(unsigned short));
    printf("unsigned long Range      => ");
    UnsignedRange(BITS(unsigned long));

    printf("SHORT-----------------------\n");
    printf("short int Range          => "); //k.A.
    SignedRange(BITS(short int));

    printf("LONG------------------------\n");
    printf("long int Range           => ");
    SignedRange(BITS(long int));
    printf("long double Range        => ");
    SignedRange(BITS(long double));
    printf("long long Range          => ");
    SignedRange(BITS(long long));

    printf("UNSIGNED-SHORT--------------\n");
    printf("unsigned short Range     => ");
    UnsignedRange(BITS(unsigned short));

    printf("UNSIGNED-LONG---------------\n");
    printf("unsigned long int Range  => ");
    UnsignedRange(BITS(unsigned long int));
    printf("unsigned long long Range => "); //wrong
    UnsignedRange(BITS(unsigned long long));

    return 0;
}