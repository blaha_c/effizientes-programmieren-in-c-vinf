//
// Created by ArbeitsPC on 13.05.2021.
//
#include "Uebung03A3.h"
//Quelle: https://aticleworld.com/c-program-to-find-the-range-of-the-data-types/
#include <stdio.h>

//Print Range of signed int
void SignedRange(unsigned int bits){
    int min = 0;
    int max = 0;
    min = - (1L <<(bits-1)); //Min value Equivalent to -2^(n-1)
    max =  ((1L <<(bits-1)) -1); //Max Value (2^(n-1)) -1
    printf("%d to %u\n",min,max);
}
//Print range of unsigned int
void UnsignedRange(unsigned int bits){
    unsigned int min = 0; //For unsigned min always 0
    unsigned long long  max = 0;
    max = ((1LLU << bits) - 1); //Equivalent to (2^n) -1
    printf(" %u to %llu\n", min, max);
}