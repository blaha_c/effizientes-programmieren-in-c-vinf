//
// Created by ArbeitsPC on 13.05.2021.
//
#ifndef EFFIZIENTES_PROGRAMMIEREN_IN_C_VINF_UEBUNG03A3_H
#define EFFIZIENTES_PROGRAMMIEREN_IN_C_VINF_UEBUNG03A3_H

#define BITS(x) (sizeof(x) * 8 )
extern void SignedRange(unsigned int bits);
extern void UnsignedRange(unsigned int bits);

#endif //EFFIZIENTES_PROGRAMMIEREN_IN_C_VINF_UEBUNG03A3_H