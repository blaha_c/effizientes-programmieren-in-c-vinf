//
// Created by ArbeitsPC on 12.05.2021.
//
#include "Uebung03A1.h"
#include <stdio.h>

int main() {
    printf("Palindrom: %1d\n", isPalNum(7));
    printf("Palindrom: %1d\n", isPalNum(1331));
    printf("Palindrom: %1d\n", isPalNum(1234321));
    printf("Palindrom: %1d\n", isPalNum(28055));
    return 0;
}