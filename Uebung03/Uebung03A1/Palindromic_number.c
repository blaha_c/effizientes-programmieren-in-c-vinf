//
// Created by ArbeitsPC on 12.05.2021.
//
#include "Uebung03A1.h"
#include <stdio.h>

//Break down into the individual digits and then add backwards.
int isPalNum(unsigned int aNum){
    int remainder, sum = 0, temp = aNum;

    while(temp > 0){
        remainder = temp % 10;
        sum = (sum * 10) + remainder;
        temp /= 10;                     //The decimal place is truncated by the data type int.
    }
    if(sum == aNum){
        return 1;
    }else {
        return 0;
    }
}