# Effizientes Programmieren in C VInf
Effizientes Programmieren in C/C++ mit Übungen von Peter Stöhr. 
Programmiert von Blaha Christian Martin.
## Markdown
Dokumentationen Markdown 
* [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
* [GitLab Markdown](https://docs.gitlab.com/ee/user/markdown.html)
* [Markdown Style Guide for GitLab](https://about.gitlab.com/handbook/markdown-guide/)
* [GitHub Flavored Markdown](https://docs.github.com/en/github/writing-on-github)
* [Documentation for GitBook](https://gitbookio.gitbooks.io/documentation/content/format/markdown.html)
## Dokumentation zu den Aufgaben
### Fehlerhafte, unvollständige und ungelöste Aufgaben
* Uebung03A3: Wertebereich teilweise nicht richtig.
* Uebung04A1: Als Elemente werden nur Integer und keine anderen Typen wie chars verwendet.
* Uebung04A1: Verwendung einer einfachen Integervariablen anstatt eines ein-elementigen Arrays.
* Uebung04A1: Verwendung von free(), Abfangen von Exception bei der Speicherbeschaffung.
* Uebung05A2: Wirklich effizienteste Funktion.
* Uebung05C1: Nur 1 von 3 Lösungen, ist meine Lösung überhaupt richtig
* Uebung06A3: Rückwärtsausgabe mit fseek(), Verwendung Arguments
* Uebung07A3: Unregelmäßiges Auftreten eines falschen Exitcodes.
* Uebung08A2: Ungelöst: Erstellen einer Umgebungsvariablen -> wenig Prüfungsrelevant
* Ueubng08A3: Ungelöst: Dateien zippen/entzippen 
* Uebung09A1P2: Komme nicht auf den richtigen Pfad der Pointer
### Aufgabestellung
* Uebung01A1: Ein C-Programm zum Laufen bringen
* Uebung01A2: Ein komplexeres C++-Programm zum Laufen bringen
* Uebung01A3: Ein Sortieralgorithmus zum Laufen bringen
* Uebung02A1: Gleiche Abfolgen in einem Array finden
* Uebung02A2: Primzahlen effizient berechnen
* Uebung03A1: Zahlenpalindrom (symmetrische Zahlen)
* Uebung03A2: Shuffeln eines Arrays
* Uebung03A3: Mögliche Datentypen und deren Speicherverbrauch, Wertebereich
* Uebung04A1: Häufigstes Element in einem Array finden
* Uebung04A2: Umrechnung von römischen Zahlen in dezimale Zahlen
* Uebung05A1: String an bestimmten Stellen teilen
* Uebung05A2: Min/Max in einem Array suchen
* Uebung05A3: Beliebig viele Strings konkatenieren
* Uebung05C1: Veränderung eines Zeichen, um richtigen Output zu erzeugen
* Uebung06A1P1: Suchen von Sequenzen in einem String
* Uebung06A1P2: Überprüfen von Strings nach Symmetrie
* Uebung06A1P3: Aufbau eines Strings/Einfügen eines Substrings in einen String
* Uebung06A1P4: Entfernen eines Chars in einem String
* Uebung06A2: Berechnung fröhlicher Zahlen
* Uebung06A3: Einlesen/Ausgabe einer Textdatei, Verwendung von Arguments
* Ubunng06C1: 2 Zahlengruppen eines Arrays sollen auf Summengleichheit überprüft werden, Verbot von Schleifen
* Uebung07A1: Leerzeichen aus einem String entfernen
* Uebung07A2: Einfach oder mehrfach vorkommende Zeichen eines Strings kodieren
* Uebung07A3: String mit "run length compression" komprimieren
* Uebung08A1: Komprimierten String aus Uebung07A3 dekomprimieren.
* Uebung08A2: Programm für Komandozeile schreiben
* Uebung08A3: Verschiedene Textdateien zippen/entzippen
* Uebung09A1: Pointerübung mit C-Puzzles
* Uebung09A2: Strings mit verschiedenen Längen sortieren
* Uebung10A1: Binärbaum in einem Array abbilden
### Lernfortschritte/Lektionen in C/C++
* Uebung01A1
* Uebung01A2
* Uebung01A3
  * Übergabe eines Arrays an eine Funktion
* Uebung02A1
  * Bool
  * Effiziente Unterfunktion
* Uebung02A2
  * Länge eines zur Kompilierzeit ertellten Arrays berechnen
  * Return eines Arrays
  * Globale Variablen im Header
  * Speicher mit `malloc()` allokieren
* Uebung03A1
* Uebung03A2
  * Funktionsreihenfolge, wenn kein Headereintrag (Uebung03A2)
  * (besseres) Random in C
  * Timestamp
  * Micro- und Nanosekunden
  * Endlosschleife
* Uebung03A3
  * Speicherplatzverbrauch Datentypen
  * Mögliche Datentypen
  * Wertebereich der Datentypen
* Uebung04A1
  * Datentyp: `size_t`
  * Erweitern von allokierten Speicher mit `realloc()` (ArrayList)
  * Verlinkung zu Programmcode eines assoziatives Arrays
  * Übergabe eines Pointers an eine Funktion
  * Länge eines dynamisch allokierten Arrays kann man ohne cnt nicht berechnen
* Uebung04A2
  * Länge eines Strings mit `strlen()` berechnen
  * Char erzeugen (Array, Pointer)
* Uebung05A1
  * String teilen mit Separator und `strtok()`
  * String kopieren mit `strcpy()`
* Uebung05A2
* Uebung05A3
  * Konkatenation (Verbindung) von Strings mit `strcat()`
  * beliebige Anzahl an Parameter/Argumenten in Funktionen
  * do-while-Schleife
* Uebung05C1
* Uebung06A1
  * Durchlaufen eines Strings mit Schlusssequenz '\0'
  * String aufbauen
  * String returnen (siehe Uebung02A2)
  * Teile eines String kopieren mit strncpy()
  * mit Pointer rechnen
* Uebung06A2
  * `printf` von `bool`
  * return `bool`
* Uebung06A3
  * Einlesen von Datei mit `fgets()`
  * Pfadangabe mit `fopen()`
  * Returncodes: `EXIT_SUCCESS`, `EXIT_FAILURE`
  * Exception werfen mit `perror()`
  * (Verwendung von Arguments - noch nicht implementiert)
* Uebung06C1
  * if-else Kurzschreibweise
  * Rekursion
  * Globale Variablen in einer C-Datei
* Uebung07A1
  * while()-loop mit `=` als Bedingung
* Uebung07A2
  * Iterieren eines Strings ohne `strlen()` sondern mit `Str[i]!='\0';` bzw. `while(str[i])`
  * Case Sensitivity mit `tolower()`/`toupper()` oder eigener Implementierung
* Uebung07A3
  * Ausgabe von Strings mit `puts()`
  * Array mit Strings erstellen
* Uebung08A1
  * Anführungszeichen (") in einem String
* Uebung08A2
* Uebung08A3
* Uebung09A1
  * Pointer von mehrdimensionalen Arrays
*Uebung09A2
  * Verwendung der Funktion `qsort()` zum Sortieren
  * Casten von einfachen Pointern (`*`) zu doppelten Pointern (`**`)
*Uebung10A1
  * Einsatz von 2D-Arrays und Doppelpointern
### Mögliche Zusatzaufgaben
* Uebung02A1: konkreten Abfolge ausgeben
* Uebung02A2: Die Längenverwaltung eines Array mit einem Struct machen
* Uebung03A1: Palidrom Umsetzung über Stringkonvertierung
* Uebung03A2: richtiges Random machen (Verschieden bei vielen Aufrufe hintereinander)
* Uebung04A1: Gleichhäufige Elemente ausgeben, assoziatives Array verwenden
* Uebung04A2: Umrechnung von Dezimale Zahlen in römische Zahlen
* Uebung06A1P1: Case Sensitivity beachten.
* Uebung06C1: Keine globale Variable benutzen.
* Uebung07A1: 2. Lösung mit for-loop und aufbauendem String
* Uebung07A2: Berücksichtigung von Sonderzeichen/Unicode (ÄaÖö usw.)
* Uebung07A3: Statt eines Char als Zahl eine Dezimalzahl in den komprimieren String schreiben
* Uebung10A1: Umsetzung mit einem Array aus structs.
### Fehler bei der Aufgabenstellung
* Uebung05A3: Falscher Funktionsname strcatAll() statt strcalAll()
* Uebung05C1: Link ist down
* Uebung07A3: "daß" statt "dass"
### Meine Coding Convention
* Dateinamen sollten in Englisch sein, beginnen mit Großbuchstaben und mehrere Wörter werden mit "_" getrennt
* Die Dokumentation ist in Deutsch
* Der Autoheader der IDE wird erhalten
* Erst System-headers `#include <stdio.h>`, danach Projekt-headers `#include "Uebung01A1.h`
* Alfernummerische Sortierung der Header
* Kommentierungen sind in Englisch
* Längere Kommentare sind /* *** */ zu halten
* main() ohne Input wird als main(void) angegeben???
* Camelcase für Funktions- und Variablennamen
* `*` ist kein Datentyp und daher direkt vor der Variable/Funktion
* Keine eigene Zeile für öffnende geschweiften Klammern "{"
* Keine Leerzeichen vor und nach geschweiften Klammern "{"
* Keine Leerzeichen vor und nach öffnenden Klammern "("
* Auf einem Komma "," folgt ein Leerzeichen
## Erstellung eines neuen Projekts
Das Projekt wurde in [GitLab](https://about.gitlab.com/) erstellt.
Als IDE wird [CLion](https://www.jetbrains.com/de-de/clion/) von JetBrains, Version: `CL-203.7148.70` verwendet.
Als Lizenz dient die Stundentenlizenz über die HAW-Hof E-Mail-Adresse.
Damit das C-Projekt in CLion erkannt wird muss bereits im GitLab-Projekt eine **.h-Datei** vorhanden sein.
Jedes Projekt kann nur _**einmal**_ von CLion erfolgreich aus dem Git geklont werden, da ansonsten der [Dialog](https://www.jetbrains.com/help/clion/creating-new-project-from-scratch.html#import-prj) zur Erstellung der Metadaten nicht mehr getriggert wird.
### Clonen eines gits:
`Git > Clone... > Repository URL`
Namen können nur einmal verwendet werden siehe: [Erstellung des Projekts](#Erstellung-des-Projekts).
### Einbinden in CLion
Durch die `.h-Datei` wird folgender [Dialog](https://www.jetbrains.com/help/clion/creating-new-project-from-scratch.html#import-prj) getriggert.
Dieser erstellt die Metadaten der IDE.
Eine hierarische Form kann in CMake (CLion) durch `Subprojects` umgesetzt werden.
Dies ermöglicht mehrere main() in einem Projekt und individuelle Einstellungen für jede einzelne main() 
Ferne hat man anstelle einer großen `CMakeLists.txt`-Auflistung viele kurze `CMakeLists.txt`-Auflistungen in jedem `Subproject`.
Hier ein [Beispielprojekt](https://github.com/anastasiak2512/Calendar) für `Subprojecs`.
Ich empfehle die Verzeichnisstruktur in der IDE anzulegen und anschließend ins Git zu pushen.
Dies hat folgenden Hintergrund: Erstellt man die Verzeichnisstruktur in GitLab (Web IDE) wird bei jedem neuen Verzeichnis eine leere `.gitkeep`-Datei erstellt.
Beim Clonen wird diese dann leider mitgeklont.
Die Ordnerstruktur in meinem Projekt:
```
├── effizientes-programmieren-in-c-vinf
│   ├── Uebung01
│   │   ├── Uebung01A1
│   │   │   ├── CMakeLists.txt
│   │   │   └── Uebung01A1.c
│   │   ├── Uebung01A2
│   │   │   ├── CMakeLists.txt
│   │   │   ├── main.cpp
│   │   │   ├── SubClass.cpp
│   │   │   ├── SubClass.h
│   │   │   ├── SuperClass.cpp
│   │   │   └── SuperClass.h
│   │   ├── Uebung01A3
│   │   │   ├── CMakeLists.txt
│   │   │   ├── doSort.c
│   │   │   ├── doSort.h
│   │   │   └── main.c
│   │   └── CMakeLists.txt
│   ├── Uebung02
│   │   ├── Uebung02A1
│   │   │   ├── CMakeLists.txt
│   │	│   └── Uebung02A1.c
│   │	└── CMakeLists.txt
...
│   ├── .gitignore
│   ├── CMakeLists.txt
│   └── README.md
```
### CMakeLists.txt
In der `CMakeLists.txt` kann man `include_directories` weglassen.
Bei den Subprojekten kann man ebenfalls `cmake_minimum_required(VERSION 3.17)` weglassen.
Mit `#` kommentiert man Zeilen aus.
#### CMakeLists.txt (Standard):
```
cmake_minimum_required(VERSION 3.17)
project(aufbauprojekt C)

set(CMAKE_C_STANDARD 11)

include_directories(.)

add_executable(aufbauprojekt main.c)
```
#### CMakeList.txt (obere Hierachie):
```
cmake_minimum_required(VERSION 3.17)
project(effizientes_programmieren_in_c_vinf C)

set(CMAKE_C_STANDARD 11)

include_directories(.)

add_subdirectory(Uebung01)
add_subdirectory(Uebung02)
#add_subdirectory(Uebung03)
#usw. ...
```
#### Uebung01/CMakeList.txt (mittlere Hierachie):
```
add_subdirectory(Uebung01A2)
add_subdirectory(Uebung01A3)
#usw. ...
```
#### Uebung01/CMakeList.txt (untere Hierachie):
```
add_executable(Uebung01A3 main.c doSort.c doSort.h)
```
#### Mehere Ordner/Dateien
```
include_directories(.)
include_directories(O1)
include_directories(O2)

add_executable(aufbau4 O1/test4.c O2/test4.c O2/test4.h test4.c test4.h)
```
### .gitignore
Zum gitignore habe ich alle Dateien hinzugefügt, die nur für die IDE benötigt werden, wie z. B. alle `CMakeLists.txt`.
Beim initialen Erstellen eines Projekts muss man alle Dateien auswählen und über das Kontextmenü 2x hinzufügen. Es sollte ein Dialog getriggert werden der eine .gitignor-Datei erstellt.
Unter `Commit > Unversionde Files > right click > Add to .gitignore > .gitignore` kann man Dateien zum .gitignore hinzufügen.
Die Dateien kann man auch manuel in die `.gitignore`-Datei eintragen.
Eine Erklärung zum gitignore von CLion findet sich unter: [gitignore](https://www.jetbrains.com/help/clion/working-with-git-tutorial.html#ignore)
## Bedinung von CLion/GitLab
### Öffenen eines Projekts
Zum Öffnen eines Projekts unter `File > Open...` einfach das Root-Verzeichnis auswählen.
### Fehlermeldung: "This file does not belong to any target"
Fehlerbehebung: "Veränderung" der in der Hierarchie am höchsten gelegenen `CMakeList.txt`, damit der Dialog `Reload Changes` getriggert wird.
### Bedeutung der Dateifarben
Bedeutung der [Dateifarben](https://www.jetbrains.com/help/phpstorm/file-status-highlights.html) in JetBrain.
In meinem Fall: Grün - dem VCS (git) hinzugefügt.
Rot - dem VCS nicht hinzugefügt (Unversioned Files). Hellgrün - dem .gitignore hinzugefügt.
### Find and Replace
`CTRL + Shift + R` oberes Suchfeld den gesuchten Namen eingeben, unten den Namen mit dem man ersetzen will.
Alle Einträge markieren und `Replace` drücken.
Fenster mit `ESC` schließen.
### Löschen eines Project in GitLab
`Settings > General > Advanced > Delete project`