//
// Created by ArbeitsPC on 16.06.2021.
//
#ifndef EFFIZIENTES_PROGRAMMIEREN_IN_C_VINF_UEBUNG04A2_H
#define EFFIZIENTES_PROGRAMMIEREN_IN_C_VINF_UEBUNG04A2_H

extern int primitiveRomanNumeralsAdditionRule(char * n);
extern int RomanNumeralsOfficialSubtractionRule(char * n);
extern int RomanNumeralsExtendedSubtractionRule(char * n);

#endif //EFFIZIENTES_PROGRAMMIEREN_IN_C_VINF_UEBUNG04A2_H
