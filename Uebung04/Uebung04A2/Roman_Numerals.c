//
// Created by ArbeitsPC on 16.06.2021.
//
#include "Uebung04A2.h"
#include <string.h>
#include <stdbool.h>

//Exercise a) - roman numerals with addition rule
int primitiveRomanNumeralsAdditionRule(char * n){
    char romanNumerals[] = "IVXLCDM";
    int romanValue[] = {1, 5, 10, 50, 100, 500, 1000};
    int cnt = 7;
    size_t len = strlen(n);
    int sum = 0;

    //Easy way
    /*
    for(int i = len - 1; i > -1; i--){
        for(int j = 0; j < cnt; j++) {
            if (n[i] == romanNumerals[j]) {
                sum += romanValue[j];
                break;
            }
        }
    }
    */

    //Efficient way
    int j = 0;
    for(int i = len - 1; i > -1; i--){
        while(j < cnt) {
            if (n[i] == romanNumerals[j]) {
                sum += romanValue[j];
                break;
            }else{
                ++j;
            }
        }
    }
    return sum;
}

//Exercise b) - roman numerals with official subtraction rule
int RomanNumeralsOfficialSubtractionRule(char * n){
    char romanNumerals[] = "IVXLCDM";
    int romanValue[] = {1, 5, 10, 50, 100, 500, 1000};
    int cnt = 7;
    size_t len = strlen(n);
    int previousIndex = 0;
    int sum = 0;

    for(int i = len - 1; i > -1; i--){
        for(int j = 0; j < cnt; j++) {
            if (n[i] == romanNumerals[j]) {
                if(j < previousIndex){
                    sum -= romanValue[j];
                }else{
                    sum += romanValue[j];
                }
                previousIndex = j;
                break;
            }
        }
    }
    return sum;
}

//Exercise c) - roman numerals with extended subtraction rule
int RomanNumeralsExtendedSubtractionRule(char * n){
    char roman_numerals[] = "IVXLCDM";
    int roman_value[] = {1, 5, 10, 50, 100, 500, 1000};
    int cnt = 7;
    size_t len = strlen(n);
    int previousIndex = 0;
    int sum = 0;
    bool subtractionFlag = false;

    for(int i = len - 1; i > -1; i--){
        for(int j = 0; j < cnt; j++) {
            if (n[i] == roman_numerals[j]) {
                if(j < previousIndex){
                    sum -= roman_value[j];
                    subtractionFlag = true;
                }else if(subtractionFlag && j == previousIndex){
                    sum -= roman_value[j];
                }else{
                    sum += roman_value[j];
                    subtractionFlag = false;
                }
                previousIndex = j;
                break;
            }
        }
    }
    return sum;
}