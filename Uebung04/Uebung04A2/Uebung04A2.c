//
// Created by ArbeitsPC on 16.06.2021.
//
#include "Uebung04A2.h"
#include <stdio.h>

int main() {
    //Exercise a)
    char * romanNumber1 = "XXXXII";     //=42
    char * romanNumber2 = "MCCXXXIIII"; //=1234
    int a = primitiveRomanNumeralsAdditionRule(romanNumber1);
    int b = primitiveRomanNumeralsAdditionRule(romanNumber2);
    printf("%s = %4d\n",romanNumber1, a);
    printf("%s = %4d\n\n",romanNumber2, b);

    //Exercise b)
    char * romanNumber3 = "IV"; //=4
    char * romanNumber4 = "IX"; //=9
    char * romanNumber5 = "XL"; //=40
    char * romanNumber6 = "XC"; //=90
    int c = RomanNumeralsOfficialSubtractionRule(romanNumber3);
    int d = RomanNumeralsOfficialSubtractionRule(romanNumber4);
    int e = RomanNumeralsOfficialSubtractionRule(romanNumber5);
    int f = RomanNumeralsOfficialSubtractionRule(romanNumber6);
    printf("%s = %4d\n",romanNumber3, c);
    printf("%s = %4d\n",romanNumber4, d);
    printf("%s = %4d\n",romanNumber5, e);
    printf("%s = %4d\n\n",romanNumber6, f);

    //Exercise c)
    char * romanNumber7 = "IIX";    //=8
    char * romanNumber8 = "CCM";    //=800
    //Example variations on wikipedia:
    char * romanNumber9 = "IIL";    //=48
    char * romanNumber10 = "XLVIII";//=48
    char * romanNumber11 = "IIC";   //=98
    char * romanNumber12 = "XCVIII"; //=98
    char * romanNumber13 = "CMXC";  //=990
    //Own examples
    char * romanNumber14 = "MCCMXLIIIX";    //=1847
    char * romanNumber15 = "IID";           //=498
    printf("%s = %4d\n",romanNumber7, RomanNumeralsExtendedSubtractionRule(romanNumber7));
    printf("%s = %4d\n",romanNumber8, RomanNumeralsExtendedSubtractionRule(romanNumber8));
    printf("%s = %4d\n",romanNumber9, RomanNumeralsExtendedSubtractionRule(romanNumber9));
    printf("%s = %4d\n",romanNumber10, RomanNumeralsExtendedSubtractionRule(romanNumber10));
    printf("%s = %4d\n",romanNumber11, RomanNumeralsExtendedSubtractionRule(romanNumber11));
    printf("%s = %4d\n",romanNumber12, RomanNumeralsExtendedSubtractionRule(romanNumber12));
    printf("%s = %4d\n",romanNumber13, RomanNumeralsExtendedSubtractionRule(romanNumber13));
    printf("%s = %4d\n",romanNumber14, RomanNumeralsExtendedSubtractionRule(romanNumber14));
    printf("%s = %4d\n",romanNumber15, RomanNumeralsExtendedSubtractionRule(romanNumber15));
    return 0;
}