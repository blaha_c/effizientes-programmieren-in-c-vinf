//
// Created by ArbeitsPC on 11.06.2021.
//
#include "Uebung04A1.h"
#include <stdio.h>

int main() {
    int aArray[] = {-12, 23, 20, 20, 32, 56, 5, -12, 1, 1, 1};
    int cnt = sizeof(aArray)/sizeof(int);
    int mostCommonElement = findMostCommonElement(aArray, cnt);
    printf("Most common element: %3d \n", mostCommonElement);
    return 0;
}