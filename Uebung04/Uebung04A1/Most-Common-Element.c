//
// Created by ArbeitsPC on 11.06.2021.
//
#include "Uebung04A1.h"
#include <stdlib.h>
//1. trial: associative array -> too complex for programming.
//https://lawlessguy.wordpress.com/2016/02/11/a-simple-associative-array-library-in-c/
//https://github.com/jimlawless/map_lib

void addElement(int value, int * element, int * count, int * cntOfElement){
    //You can't resize an array in C, their size is set at compile time.
    //You can resize dynamically-sized array-like memory region, you have to use memory allocation.
    //Not dealing with realloc() failing because memory could not be allocated.
    //No free()
    size_t expanding_size = sizeof(int);
    element = (int *)realloc(element, expanding_size);
    count = (int *)realloc(count, expanding_size);

    int indexLastElement = cntOfElement[0];
    ++cntOfElement[0];
    element[indexLastElement] = value;
    count[indexLastElement] = 1;
}

int highestValue(int cnt, int * count){
    int indexHighest = 0;
    for(int i = 1; i < cnt; i++){
        if(count[i] > count[indexHighest]){
            indexHighest = i;
        }
    }
    return indexHighest;
}

int findMostCommonElement(int *aArray, int cnt){
    int *element = (int *)malloc(sizeof(int));
    int *count = (int *)malloc(sizeof(int));
    int *cntOfElement = (int *)malloc(sizeof(int));
    element[0] = aArray[0];
    count[0] = 1;
    //Can it be done smarter without the 1-element-array cntOfElement and without using a struct?
    cntOfElement[0] = 1;

    for(int i = 1; i < cnt; i++){
        //Would always calculate with the size of the pointer and not with the size of the referencing array.
        //int lengthElement = sizeof(element)/sizeof(int);
        for(int j = 0; j < cntOfElement[0]; j++){
            if(aArray[i] == element[j]){
                count[j] += 1;
                break;
            }
            if(j == cntOfElement[0] - 1){
                addElement(aArray[i], element, count, cntOfElement);
                //Without break, there is one loop iteration too many due to +=1 in addElement().
                break;
            }
        }
    }

    int index = highestValue(cntOfElement[0], count);
    return element[index];
}